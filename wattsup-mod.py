#!/usr/bin/env python
import os, serial, sys, signal
import datetime, time
import argparse

EXTERNAL_MODE = 'E'
INTERNAL_MODE = 'I'
TCPIP_MODE = 'T'
FULLHANDLING = 2


class WattsUp(object):
    def __init__(self, port, interval):
        self.s = serial.Serial(port, 115200 )
        self.interval = interval

    def log(self, logfile = None):
        if logfile:
            self.logfile = logfile
            o = open(self.logfile,'w')
        line = self.s.readline()
        while True:
            if line.startswith( '#d' ):
                tstamp = str(long(time.time()))+','
                o.write(tstamp+line)
                line = self.s.readline()

def signal_term_handler(signal, frame):
    sys.exit()

signal.signal(signal.SIGINT, signal_term_handler)

def main(args):
    if not args.port:
        args.port = '/dev/ttyUSB0'
    if not os.path.exists(args.port):
        print 'Serial port %s does not exist.' % args.port
        print 'Default ports are /dev/ttyUSB0 for Linux'
        exit()
    meter = WattsUp(args.port, args.interval)
    meter.log(args.outfile)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Get data from Watts Up power meter.')
    parser.add_argument('-s', '--sample-interval', dest='interval', default=1.0, type=float, help='Sample interval (default 1 s)')
    parser.add_argument('-p', '--port', dest='port', default=None, help='USB serial port')
    parser.add_argument('-o', '--outfile', dest='outfile', default='log.out', help='Output file')
    args = parser.parse_args()
    main(args)
